// Package settings provides a setting system with any parameters necessary to configure the runtime.
//
// All thoses parameters can be changed with flags when the binray is run.
package settings

import (
	"flag"
	log "gitlab.com/AeN_555/colorlog"
	"os"
	"path/filepath"
)

var (
	ProjectPathDirectory   string
	GeneratedPathDirectory string
	GeneratedPackageName   string
	VerboseLevel           uint
	Color                  bool
)

// To parse the flags in the command line and apply thoses settings to the runtime.
func ParseFlags() {
	flag.StringVar(&ProjectPathDirectory, "p", ".", "the path to the directory where the project is")
	flag.StringVar(&GeneratedPathDirectory, "d", "./gen", "the directory to generate in")
	flag.StringVar(&GeneratedPackageName, "o", "build", "the package name to store informations")
	flag.UintVar(&VerboseLevel, "v", 3, "verbose level (0 to 5)")
	flag.BoolVar(&Color, "c", true, "enable color")
	flag.Parse()

	applySettings()
}

// To check and apply the settings to the runtime.
func applySettings() {
	ProjectPathDirectory = filepath.Clean(ProjectPathDirectory)
	if fileInfo, err := os.Stat(ProjectPathDirectory); err == nil {
		if fileInfo.IsDir() == false {
			log.Error(nil, "Path \"", ProjectPathDirectory, "\" is not a project directory")
		} else {
			if err := os.Chdir(ProjectPathDirectory); err != nil {
				log.Error(err, "Failed to change to directory \""+ProjectPathDirectory+"\"")
			}
		}
	} else {
		log.Error(err, "Failed to check path \"", ProjectPathDirectory, "\"")
	}

	GeneratedPathDirectory = filepath.Clean(GeneratedPathDirectory)
	if _, err := os.Stat(GeneratedPathDirectory); os.IsNotExist(err) == true {
		if err = os.MkdirAll(GeneratedPathDirectory, 0755); err != nil {
			log.Error(err, "Failed to create directory \"", GeneratedPathDirectory, "\"")
		}
	}

	log.SetLevel(VerboseLevel)
	log.SetColor(Color)
}

// To display the settings.
//
// This function is used to inform the user of the current settings.
func DisplaySettings() {
	log.Information("Project path directory is \"", ProjectPathDirectory, "\"")
	log.Information("Generated path directory is \"", GeneratedPathDirectory, "\"")
}
