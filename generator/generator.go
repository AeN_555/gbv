// Package generator provides a generation of build informations into a file.
package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/gbv/build"
	"os"
	"text/template"
)

const (
	txtFile string = `// Generated file by gbv (see https://gitlab.com/AeN_555/gbv)
//
// DO NOT MODIFY OR VERSION-CONTROL IT
package {{.PackageName}}

import (
	"time"
)

const (
	Version            string    = "{{.Version}}"
	Commit             string    = "{{.Commit}}"
	UnversionedChanges bool      = {{.UnversionedChanges}}
)

var (
	Date time.Time
)

func init() {
	Date, _ = time.Parse(time.UnixDate, "{{.Date}}")
}
`
)

var (
	tplFile *template.Template = template.Must(template.New("file").Parse(txtFile))
)

// To clean the generated folder.
//
// It removes and creates the package folder. The goal is to avoid old unecessary (and maybe in-conflict) generated files.
func Clean(rootPathDirectory string, packageName string) {
	folderPath := rootPathDirectory + string(os.PathSeparator) + packageName
	if err := os.RemoveAll(folderPath); err != nil {
		log.Error(err, "Failed to remove \"", folderPath, "\"")
	}

	if err := os.MkdirAll(folderPath, 0755); err != nil {
		log.Error(err, "Failed to make directory \"", folderPath, "\"")
	}
}

// To generate the package.
func Generate(rootPathDirectory string, packageName string) {
	buildData := build.GitData()

	filePath := rootPathDirectory + string(os.PathSeparator) + packageName + string(os.PathSeparator) + packageName + ".go"
	file, err := os.Create(filePath)
	if err != nil {
		log.Error(err, "Failed to create file \"", filePath, "\"")
	}
	defer file.Close()

	data := struct {
		*build.Data

		PackageName string
	}{
		Data: buildData,

		PackageName: packageName,
	}
	if err := tplFile.ExecuteTemplate(file, "file", data); err != nil {
		log.Error(err, "Failed to generate \"file\" template in \"", file.Name(), "\"")
	}
}
