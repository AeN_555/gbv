// Package gbv is a binary to generate build informations like version and date.
//
// It provides usefull build informations from Git for the application that will use it.
package main

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/gbv/generator"
	"gitlab.com/AeN_555/gbv/settings"
)

// To define the entry point of the binary.
func main() {
	settings.ParseFlags()
	log.Information("Starting gbv")

	settings.DisplaySettings()

	log.Verbose("Cleaning generated directory")
	generator.Clean(settings.GeneratedPathDirectory, settings.GeneratedPackageName)
	log.Verbose("Generated directory cleaned")

	log.Verbose("Generating ressource")
	generator.Generate(settings.GeneratedPathDirectory, settings.GeneratedPackageName)
	log.Information("Ressource generated")

	log.Information("Ending gbv")
}
