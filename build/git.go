// Package build provides build informations from versionning software.
package build

import (
	log "gitlab.com/AeN_555/colorlog"
	"os/exec"
	"strings"
	"time"
)

type (
	Data struct {
		Version            string
		Commit             string
		UnversionedChanges bool
		Date               string
	}
)

const (
	UnknownData string = "unknown"
)

// Get build data from Git repository (need git tools)
func GitData() *Data {
	res := &Data{
		Version:            UnknownData,
		Commit:             UnknownData,
		UnversionedChanges: true,
		Date:               time.Now().Format(time.UnixDate),
	}

	cmd := exec.Command("git", "describe", "--abbrev=0")
	out, err := cmd.Output()
	if err != nil {
		log.Error(err, "Failed to execute command \""+cmd.String()+"\"")
	}
	res.Version = string(out)
	res.Version = res.Version[:len(res.Version)-1]

	cmd = exec.Command("git", "rev-parse", "--verify", "HEAD")
	out, err = cmd.Output()
	if err != nil {
		log.Error(err, "Failed to execute command \""+cmd.String()+"\"")
	}
	res.Commit = string(out)
	res.Commit = res.Commit[:len(res.Commit)-1]

	cmd = exec.Command("git", "status", "-s")
	out, err = cmd.Output()
	if err != nil {
		log.Error(err, "Failed to execute command \""+cmd.String()+"\"")
	}
	res.UnversionedChanges = (strings.Count(string(out), "\n") > 1)

	return res
}
