# gbv (Go Build Version)

![Logo](./logo.png "gbv logo")

gbv is a Go program to generate build informations like version and date for other Go programs.

### Requierements

The package depends on [ColorLog](https://gitlab.com/AeN_555/colorlog "Link to ColorLog").

Obviously, it also requires [Go](https://golang.org/ "Link to Golang") itself.
But, at least, version 1.4 to be able to run `go generate` command.

### Installation

gbv uses Go module system, so you don't need to install it to be able to use it.
Just add the necessary comment in your main package like in the example below.

### Example

In "./myProject.go":

```go
package main

import (
	"myProject/gen/build"
	"fmt"
	"time"
)

//go:generate go run gitlab.com/AeN_555/gbv
func main() {
	fmt.Println("Git lasted tag is: ", build.Version)
	fmt.Println("Current Git commit SHA1 is: ", build.Commit)
	fmt.Println("Is there uncommited / modified file in the current Git repository at build time? ", build.UnversionedChanges)
	fmt.Println("Time build is: ", build.Date.Format(time.UnixDate))
}
```

### Build and result

```bash
$ go generate
```

![Result of the generation](./resultGeneration.png)

```bash
$ go build .
$ ./myProject
```

![Result of the example](./result.png)

### Usage

```
Usage of grg:
  -c    enable color (default true)
  -d string
        the directory to generate in (default "./gen")
  -o string
        the package name to store informations (default "build")
  -p string
        the path to the directories where the project is (default ".")
  -v uint
        verbose level (0 to 5) (default 3)
```

Notice that you can use thoses flags in the `go:generate` comment to specify your project.

### Warnings and cautions

gbv uses a specified directory to generate code so this directory will be completly erased at start-up (usually when `go generate` is run). Only the package is removed not the entirely generated directory. For example, with default values, directory "gen" is never deleted (only created if needed) but the directory "build" is completly deleted et created everytime.

### Supported versioning softwares

For now, only Git is supported. Very few changes are necessary to implement another versioning software. I don't use the others so I cannot verify and test them if I implement them.
